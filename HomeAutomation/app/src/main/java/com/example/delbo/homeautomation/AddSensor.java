package com.example.delbo.homeautomation;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AddSensor extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    EditText hw_id, sensorName;
    Button addSensorButton;
    boolean sensorAdded = false;
    TextView userNav, emailNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sensor);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);

        userNav = (TextView) header.findViewById(R.id.UserNav);
        emailNav = (TextView) header.findViewById(R.id.UserEmailNav);

        userNav.setText("Logged in as:");
        emailNav.setText(LogInScreen.user_email);

        hw_id = (EditText) findViewById(R.id.hw_id);
        sensorName = (EditText) findViewById(R.id.sensor_name);
        addSensorButton = (Button) findViewById(R.id.addSensorButton);


        addSensorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isNetworkAvailable())
                {
                    String HW_ID = hw_id.getText().toString();
                    String SensorName = sensorName.getText().toString();

                    AddSensor.addSensor task = new AddSensor.addSensor();
                    task.execute(new String[]{"https://iot.bsad.dk/api/sensor/store"
                            + "?hw_id="+HW_ID+"&name="+SensorName + "&token=" + LogInScreen.token});
                }
                else
                {
                    Toast.makeText(AddSensor.this, "Error - NO Internet", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_sensor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.add_sensor) {

            Intent intent = new Intent(getBaseContext(),AddSensor.class);
            startActivity(intent);

        } else if (id == R.id.settings) {

        } else if (id == R.id.main_View) {

            Intent intent = new Intent(getBaseContext(),MainActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_share) {
            Intent intent = new Intent(getBaseContext(),StartScreen.class);
            startActivity(intent);

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private class addSensor extends AsyncTask<String, Void, Boolean> {


        // ProcessBox while data is read
        ProgressDialog dialog = new ProgressDialog(AddSensor.this);


        @Override
        protected void onPreExecute() {
            dialog.setMessage("Adding Sensor to System...");
            dialog.show();
        }

        String text = "";

        @Override
        protected Boolean doInBackground(String... urls) {
            // Read from web to InputStream
            InputStream inputStream;
            for (String url1 : urls) {
                try {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost(url1);
                    HttpResponse response = client.execute(post);
                    inputStream = response.getEntity().getContent();

                } catch (IOException e) {
                    //Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    return false;
                }
                // End of read from web to InputStream

                // Convert from InputStream to String Text
                BufferedReader reader;

                try {
                    reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        text += line + "\n";
                    }
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Log.d("Text er ", text);
            // Convert from Text to JSON and search for ID and status
            try {
                JSONObject json = new JSONObject(text);
                // Checks if the agreement has been created
               /* if (json.getString("status").equals("ok"))
                {
                    token = json.getString("token");
                    loggedIn = true;
                }

                else
                {
                    loggedIn = false;
                }*/
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute (Boolean result){
            if (result == true){
                Toast.makeText(AddSensor.this, "Sensor Added To System", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getBaseContext(),AddSensor.class);
                startActivity(intent);
                /*if(sensorAdded == true)
                {

                }
                else
                {
                    Toast.makeText(AddSensor.this, "Error - try again", Toast.LENGTH_SHORT).show();
                }*/
            }
            else{
                Toast.makeText(AddSensor.this, "Error", Toast.LENGTH_SHORT).show();
            }
            dialog.dismiss();
        }
    }
}
