package com.example.delbo.homeautomation;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class NewUser extends AppCompatActivity {

    EditText nameNew, pswdNew, emailNew;
    Button createUser;
    Boolean createdUser = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);

        nameNew = (EditText) findViewById(R.id.nameNew);
        pswdNew = (EditText) findViewById(R.id.pswdNew);
        emailNew = (EditText) findViewById(R.id.emailNew);
        createUser = (Button) findViewById(R.id.createUserButton);

        createUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = nameNew.getText().toString();
                String password = pswdNew.getText().toString();
                String email = emailNew.getText().toString();

                if(isNetworkAvailable())
                {
                    NewUser.createUser task = new NewUser.createUser();
                    task.execute(new String[]{"https://iot.bsad.dk/api/auth/signup"
                            + "?name="+name+"&email="+email + "&password=" + password});
                }

            }
        });
    }
    private class createUser extends AsyncTask<String, Void, Boolean> {


        // ProcessBox while data is read
        ProgressDialog dialog = new ProgressDialog(NewUser.this);


        @Override
        protected void onPreExecute() {
            dialog.setMessage("Adding Sensor to System...");
            dialog.show();
        }

        String text = "";

        @Override
        protected Boolean doInBackground(String... urls) {
            // Read from web to InputStream
            InputStream inputStream;
            for (String url1 : urls) {
                try {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost(url1);
                    HttpResponse response = client.execute(post);
                    inputStream = response.getEntity().getContent();

                } catch (IOException e) {
                    //Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    return false;
                }
                // End of read from web to InputStream

                // Convert from InputStream to String Text
                BufferedReader reader;

                try {
                    reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        text += line + "\n";
                    }
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Log.d("Text er ", text);
            // Convert from Text to JSON and search for ID and status
            try {
                JSONObject json = new JSONObject(text);
                // Checks if the agreement has been created
                if (json.getString("status").equals("ok"))
                {
                    createdUser = true;
                }

                else
                {
                    createdUser = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute (Boolean result){
            if (result == true){
                if(createdUser)
                {
                    Toast.makeText(NewUser.this, "User Created - Please Log In", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getBaseContext(),LogInScreen.class);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(NewUser.this, "Error - try again", Toast.LENGTH_SHORT).show();
                }
            }
            else{
                Toast.makeText(NewUser.this, "Error", Toast.LENGTH_SHORT).show();
            }
            dialog.dismiss();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
