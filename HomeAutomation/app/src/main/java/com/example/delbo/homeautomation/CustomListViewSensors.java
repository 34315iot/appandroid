package com.example.delbo.homeautomation;

/**
 * Created by delbo on 23/03/2017.
 */
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

public class CustomListViewSensors extends BaseAdapter{

    LayoutInflater inflater;
    List<Sensor> sensors;

    public CustomListViewSensors (Activity context, List<Sensor> sensors){
        super();

        this.sensors = sensors;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return sensors.size();
    }

    @Override
    public Object getItem(int position) {

        return sensors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // Creates the view and adds the info for the different views
    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {


        final Sensor item = sensors.get(position);

        View vi = convertView;


        if (convertView == null){
            vi = inflater.inflate(R.layout.sensors_view, null); //creates a view, which has the structure of "account_view"
        }


        final TextView SensorName = (TextView) vi.findViewById(R.id.Sensor_name);
        TextView SensorValue = (TextView) vi.findViewById(R.id.Sensor_value);
        TextView SensorUpdate = (TextView) vi.findViewById(R.id.Sensor_update);
        TextView SensorCreate = (TextView) vi.findViewById(R.id.Sensor_create);
        TextView SensorType = (TextView) vi.findViewById(R.id.Sensor_Type);
        Button SensorButton = (Button) vi.findViewById(R.id.Sensor_Button);

        // Sets the textViews for each item in the listView
        SensorName.setText("Sensor: " + item.name);
        SensorValue.setText("Value: " + item.value);
        SensorUpdate.setText("Last update: " + item.updated_at);
        SensorCreate.setText("Created at: " + item.created_at);
        SensorType.setText("Type: " + item.type);

        if(item.type.equals("switch"))
        {
            SensorButton.setVisibility(View.VISIBLE);
        }
        else
        {
            SensorButton.setVisibility(View.GONE);
        }

        SensorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Test", item.name);
                if(item.value.equals("1"))
                {
                    updateSensor task1 = new updateSensor();
                    task1.execute(new String[]{"https://iot.bsad.dk/api/sensor/valueupdate?id=" + item.hw_id
                            + "&value=0" + "&token=" + LogInScreen.token});
                    Intent zoom=new Intent(parent.getContext(), MainActivity.class);
                    parent.getContext().startActivity(zoom);
                }
                else
                {
                    updateSensor task1 = new updateSensor();
                    task1.execute(new String[]{"https://iot.bsad.dk/api/sensor/valueupdate?id=" + item.hw_id
                            + "&value=1" + "&token=" + LogInScreen.token});
                    Log.d("Sendt ", "https://iot.bsad.dk/api/sensor/valueupdate?id=" + item.hw_id
                            + "&value=1" + "&token=" + LogInScreen.token);
                    Intent zoom=new Intent(parent.getContext(), MainActivity.class);
                    parent.getContext().startActivity(zoom);
                }

            }
        });

        return vi;
    }

    private class updateSensor extends AsyncTask<String, Void, Boolean> {


        // ProcessBox while data is read
        //ProgressDialog dialog = new ProgressDialog(CustomListViewSensors.this);

       /* @Override
        protected void onPreExecute() {
            dialog.setMessage("Removing Sensor...");
            dialog.show();
        }*/

        String text = "";

        @Override
        protected Boolean doInBackground(String... urls) {
            // Read from web to InputStream
            InputStream inputStream;
            for (String url1 : urls) {
                try {
                    HttpClient client = new DefaultHttpClient();
                    HttpGet get = new HttpGet(url1);
                    HttpResponse response = client.execute(get);
                    inputStream = response.getEntity().getContent();

                } catch (IOException e) {
                   // Toast.makeText(Cu.this, e.toString(), Toast.LENGTH_SHORT).show();
                    return false;
                }
                // End of read from web to InputStream

                // Convert from InputStream to String Text
                BufferedReader reader;

                try {
                    reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        text += line + "\n";
                    }
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Log.d("Text er ", text);
            // Convert from Text to JSON and search for ID and status
            /*try {
                JSONArray json = new JSONArray(text);
                for (int i = 0; i<json.length(); i++) {
                    JSONObject jsonData = json.getJSONObject(i);
                    LogInScreen.Sensorid = jsonData.getString("id");
                    LogInScreen.Sensorhw_id = jsonData.getString("hw_id");
                    LogInScreen.Sensorname = jsonData.getString("name");
                    LogInScreen.Sensorvalue = jsonData.getString("value");
                    LogInScreen.Sensorcreated_at = jsonData.getString("created_at");
                    LogInScreen.Sensorupdated_at = jsonData.getString("updated_at");

                    Sensor sensor = new Sensor(LogInScreen.Sensorid, LogInScreen.Sensorhw_id, LogInScreen.Sensorname, LogInScreen.Sensorvalue,
                            LogInScreen.Sensorcreated_at, LogInScreen.Sensorupdated_at);
                    LogInScreen.sensors.add(sensor);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
            return true;
        }

        @Override
        protected void onPostExecute (Boolean result){
            if (result == true){

                //Toast.makeText(MainActivity.this, "Sensor Removed", Toast.LENGTH_SHORT).show();
                //updateView();

                //adapter.notifyDataSetChanged();
                //swipeRefreshLayout.setRefreshing(false);
                // Creates a new adapter for the listView
                //Toast.makeText(LogInScreen.this, "Loaded", Toast.LENGTH_SHORT).show();
            }
            else{
                //Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
           // dialog.dismiss();
        }
    }

}
