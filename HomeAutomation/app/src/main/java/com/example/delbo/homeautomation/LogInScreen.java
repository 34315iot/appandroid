package com.example.delbo.homeautomation;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LogInScreen extends AppCompatActivity {

    Button logIn;
    EditText email, password;
    public  static String user_email;
    public static String token;
    boolean loggedIn = false;
    public static String Sensorid, Sensorhw_id, Sensorname, Sensorvalue, Sensorcreated_at, Sensorupdated_at, type;
    public static ArrayList<Sensor> sensors = new ArrayList<Sensor>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_screen);

        logIn = (Button) findViewById(R.id.logInButton);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.pswd) ;

        // Disables the rotation function at the phone
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user_email = email.getText().toString();
                String pswd = password.getText().toString();

                if (isNetworkAvailable()){
                    logInTask task = new logInTask();
                    task.execute(new String[]{"https://iot.bsad.dk/api/auth/login"
                            + "?email="+user_email+"&password="+pswd});
                }
                else Toast.makeText(LogInScreen.this, "Error - NO Internet", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private class logInTask extends AsyncTask<String, Void, Boolean> {


        // ProcessBox while data is read
        ProgressDialog dialog = new ProgressDialog(LogInScreen.this);


        @Override
        protected void onPreExecute() {
            dialog.setMessage("Logging In...");
            dialog.show();
        }

        String text = "";

        @Override
        protected Boolean doInBackground(String... urls) {
            // Read from web to InputStream
            InputStream inputStream;
            for (String url1 : urls) {
                try {
                    HttpClient client = new DefaultHttpClient();
                    HttpPost post = new HttpPost(url1);
                    HttpResponse response = client.execute(post);
                    inputStream = response.getEntity().getContent();

                } catch (IOException e) {
                    //Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    return false;
                }
                // End of read from web to InputStream

                // Convert from InputStream to String Text
                BufferedReader reader;

                try {
                    reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        text += line + "\n";
                    }
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Log.d("Text er ", text);
            // Convert from Text to JSON and search for ID and status
            try {
                JSONObject json = new JSONObject(text);
                // Checks if the agreement has been created
                if (json.getString("status").equals("ok"))
                {
                    token = json.getString("token");
                    loggedIn = true;
                }

                else
                {
                    loggedIn = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute (Boolean result){
            if (result == true){
                if(loggedIn == true)
                {
                    getSensors task1 = new getSensors();
                    task1.execute(new String[]{"https://iot.bsad.dk/api/sensor?token="+token});
                }
                else
                {
                    Toast.makeText(LogInScreen.this, "Wrong Username or Password", Toast.LENGTH_SHORT).show();
                    Log.d("Læser ikke rigtigt", result.toString());
                }
            }
            else{
				Toast.makeText(LogInScreen.this, "Fejl", Toast.LENGTH_SHORT).show();
            }
            dialog.dismiss();
        }
    }

    private class getSensors extends AsyncTask<String, Void, Boolean> {


        // ProcessBox while data is read
        ProgressDialog dialog = new ProgressDialog(LogInScreen.this);


        @Override
        protected void onPreExecute() {
            dialog.setMessage("Importing Sensors...");
            dialog.show();
        }

        String text = "";

        @Override
        protected Boolean doInBackground(String... urls) {
            // Read from web to InputStream
            InputStream inputStream;
            for (String url1 : urls) {
                try {
                    HttpClient client = new DefaultHttpClient();
                    HttpGet get = new HttpGet(url1);
                    HttpResponse response = client.execute(get);
                    inputStream = response.getEntity().getContent();

                } catch (IOException e) {
                    //Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    return false;
                }
                // End of read from web to InputStream

                // Convert from InputStream to String Text
                BufferedReader reader;

                try {
                    reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        text += line + "\n";
                    }
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Log.d("Text er ", text);
            // Convert from Text to JSON and search for ID and status
            try {
                JSONArray json = new JSONArray(text);
                for (int i = 0; i<json.length(); i++) {
                    JSONObject jsonData = json.getJSONObject(i);
                    Sensorid = jsonData.getString("id");
                    Sensorhw_id = jsonData.getString("hw_id");
                    Sensorname = jsonData.getString("name");
                    Sensorvalue = jsonData.getString("value");
                    Sensorcreated_at = jsonData.getString("created_at");
                    Sensorupdated_at = jsonData.getString("updated_at");
                    type = jsonData.getString("type");

                    Sensor sensor = new Sensor(Sensorid, Sensorhw_id, Sensorname, Sensorvalue, Sensorcreated_at, Sensorupdated_at, type);
                    sensors.add(sensor);
                }
                // Checks if the agreement has been created
                /*if (json.getString("name").equals("ok"))
                {
                    token = json.getString("token");
                    loggedIn = true;
                }

                else
                {
                    loggedIn = false;
                }*/
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute (Boolean result){
            if (result == true){
                Intent intent = new Intent(getBaseContext(),MainActivity.class);
                startActivity(intent);
                // Creates a new adapter for the listView
                //Toast.makeText(LogInScreen.this, "Loaded", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(LogInScreen.this, "Error", Toast.LENGTH_SHORT).show();
            }
            dialog.dismiss();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
