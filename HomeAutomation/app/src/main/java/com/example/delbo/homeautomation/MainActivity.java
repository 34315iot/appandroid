package com.example.delbo.homeautomation;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //public static String Sensorid, Sensorhw_id, Sensorname, Sensorvalue, Sensorcreated_at, Sensorupdated_at;
    //static ArrayList <Sensor> sensors = new ArrayList<Sensor>();
    static ListView lv;
    static CustomListViewSensors adapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    TextView userNav, emailNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        updateView();

        lv = (ListView) findViewById(R.id.main_Layout);

        // Disables the rotation function at the phone
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);


        String[] values = new String[] { "Android List View",
                "Adapter implementation",
                "Simple List View In Android",
                "Create List View Android",
                "Android Example",
                "List View Source Code",
                "List View Array Adapter",
                "Android Example List View"
        };

//        Log.d("Sensor 1 ", LogInScreen.sensors.get(0).id);

        adapter = new CustomListViewSensors(this, LogInScreen.sensors);
        lv.setAdapter(adapter);

        // Define a new Adapter
        // First parameter - Context
        // Second parameter - Layout for the row
        // Third parameter - ID of the TextView to which the data is written
        // Forth - the Array of data



       lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
           @Override
           public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

               // ListView Clicked item index
               int itemPosition     = position;

               String values = lv.getItemAtPosition(itemPosition).toString();
               final String sensorId = LogInScreen.sensors.get(position).hw_id;
               String sensorName = LogInScreen.sensors.get(position).name;
               Log.d("Values er ", values);
               Log.d("SensorId er ", sensorId);


               AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
               builder
                       .setTitle("Remove Sensor: " + sensorName)
                       .setMessage("Are you sure?")
                       .setIcon(android.R.drawable.ic_dialog_alert)
                       .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                           public void onClick(DialogInterface dialog, int which) {
                               //Yes button clicked, do something

                               if(isNetworkAvailable())
                               {
                                   removeSensor task1 = new removeSensor();
                                   task1.execute(new String[]{"https://iot.bsad.dk/api/sensor/remove?token="+LogInScreen.token +
                                           "&id=" + sensorId});
                               }

                               //Toast.makeText(MainActivity.this, "Yes button pressed",Toast.LENGTH_SHORT).show();
                           }
                       })
                       .setNegativeButton("No", null) //Do nothing on no
                       .show();



               /*// ListView Clicked item value
               String  itemValue    = (String) lv.getItemAtPosition(position);

               // Show Alert
               Toast.makeText(getApplicationContext(),
                       "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
                       .show();*/
           }
       });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                updateView();
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header = navigationView.getHeaderView(0);

        userNav = (TextView) header.findViewById(R.id.UserNav);
        emailNav = (TextView) header.findViewById(R.id.UserEmailNav);

        userNav.setText("Logged in as:");
        emailNav.setText(LogInScreen.user_email);

    }

    public void updateView() {
        if(isNetworkAvailable())
        {
            LogInScreen.sensors.clear();
            getSensors task1 = new getSensors();
            task1.execute(new String[]{"https://iot.bsad.dk/api/sensor?token="+LogInScreen.token});
        }
        else Toast.makeText(MainActivity.this, "No Internet", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.add_sensor) {

            Intent intent = new Intent(getBaseContext(),AddSensor.class);
            startActivity(intent);

        } else if (id == R.id.settings) {

        } else if (id == R.id.main_View) {

            Intent intent = new Intent(getBaseContext(),MainActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_share) {
            Intent intent = new Intent(getBaseContext(),StartScreen.class);
            startActivity(intent);

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private class getSensors extends AsyncTask<String, Void, Boolean> {


        // ProcessBox while data is read
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);


        @Override
        protected void onPreExecute() {
            dialog.setMessage("Updating Sensors...");
            dialog.show();
        }

        String text = "";

        @Override
        protected Boolean doInBackground(String... urls) {
            // Read from web to InputStream
            InputStream inputStream;
            for (String url1 : urls) {
                try {
                    HttpClient client = new DefaultHttpClient();
                    HttpGet get = new HttpGet(url1);
                    HttpResponse response = client.execute(get);
                    inputStream = response.getEntity().getContent();

                } catch (IOException e) {
                    //Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    return false;
                }
                // End of read from web to InputStream

                // Convert from InputStream to String Text
                BufferedReader reader;

                try {
                    reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        text += line + "\n";
                    }
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            Log.d("Text er ", text);
            // Convert from Text to JSON and search for ID and status
            try {
                JSONArray json = new JSONArray(text);
                for (int i = 0; i<json.length(); i++) {
                    JSONObject jsonData = json.getJSONObject(i);
                    LogInScreen.Sensorid = jsonData.getString("id");
                    LogInScreen.Sensorhw_id = jsonData.getString("hw_id");
                    LogInScreen.Sensorname = jsonData.getString("name");
                    LogInScreen.Sensorvalue = jsonData.getString("value");
                    LogInScreen.Sensorcreated_at = jsonData.getString("created_at");
                    LogInScreen.Sensorupdated_at = jsonData.getString("updated_at");
                    LogInScreen.type = jsonData.getString("type");

                    Sensor sensor = new Sensor(LogInScreen.Sensorid, LogInScreen.Sensorhw_id, LogInScreen.Sensorname, LogInScreen.Sensorvalue,
                            LogInScreen.Sensorcreated_at, LogInScreen.Sensorupdated_at, LogInScreen.type);
                    LogInScreen.sensors.add(sensor);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;
        }

        @Override
        protected void onPostExecute (Boolean result){
            if (result == true){
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
                // Creates a new adapter for the listView
                //Toast.makeText(LogInScreen.this, "Loaded", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
            dialog.dismiss();
        }
    }

    private class removeSensor extends AsyncTask<String, Void, Boolean> {


        // ProcessBox while data is read
        ProgressDialog dialog = new ProgressDialog(MainActivity.this);

       /* @Override
        protected void onPreExecute() {
            dialog.setMessage("Removing Sensor...");
            dialog.show();
        }*/

        String text = "";

        @Override
        protected Boolean doInBackground(String... urls) {
            // Read from web to InputStream
            InputStream inputStream;
            for (String url1 : urls) {
                try {
                    HttpClient client = new DefaultHttpClient();
                    HttpDelete delete = new HttpDelete(url1);
                    HttpResponse response = client.execute(delete);
                    //inputStream = response.getEntity().getContent();

                } catch (IOException e) {
                    //Toast.makeText(MainActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                    return false;
                }
                // End of read from web to InputStream

                // Convert from InputStream to String Text
                BufferedReader reader;

               /* try {
                    reader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);

                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        text += line + "\n";
                    }
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
            }

            Log.d("Text er ", text);
            // Convert from Text to JSON and search for ID and status
            /*try {
                JSONArray json = new JSONArray(text);
                for (int i = 0; i<json.length(); i++) {
                    JSONObject jsonData = json.getJSONObject(i);
                    LogInScreen.Sensorid = jsonData.getString("id");
                    LogInScreen.Sensorhw_id = jsonData.getString("hw_id");
                    LogInScreen.Sensorname = jsonData.getString("name");
                    LogInScreen.Sensorvalue = jsonData.getString("value");
                    LogInScreen.Sensorcreated_at = jsonData.getString("created_at");
                    LogInScreen.Sensorupdated_at = jsonData.getString("updated_at");

                    Sensor sensor = new Sensor(LogInScreen.Sensorid, LogInScreen.Sensorhw_id, LogInScreen.Sensorname, LogInScreen.Sensorvalue,
                            LogInScreen.Sensorcreated_at, LogInScreen.Sensorupdated_at);
                    LogInScreen.sensors.add(sensor);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
            return true;
        }

        @Override
        protected void onPostExecute (Boolean result){
            if (result == true){
                Toast.makeText(MainActivity.this, "Sensor Removed", Toast.LENGTH_SHORT).show();
                updateView();

                //adapter.notifyDataSetChanged();
                //swipeRefreshLayout.setRefreshing(false);
                // Creates a new adapter for the listView
                //Toast.makeText(LogInScreen.this, "Loaded", Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
            dialog.dismiss();
        }
    }
}
