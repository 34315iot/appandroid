package com.example.delbo.homeautomation;

/**
 * Created by delbo on 23/03/2017.
 */

public class Sensor {

    String id, hw_id, name, value, created_at, updated_at, type;

    Sensor(String id, String hw_id, String name, String value, String created_at, String updated_at, String type)
    {
        this.id = id;
        this.hw_id = hw_id;
        this.name = name;
        this.value = value;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.type = type;
    }
}
